﻿using AspCourse.CustomContainer.Controllers;
using AspCourse.CustomContainer.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AspCourse.CustomContainer
{
    public static class IoCConfig
    {
        public static void ConfigureContainer()
        {
            var container = new IoCContainer();

            container.Register<IDoNothingService, DoNothingService>();
            container.Register<HomeController, HomeController>(LifeCycle.Transient);

            ControllerBuilder.Current.SetControllerFactory(new IocControllerFactory(container));
        }
    }
}
