﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspCourse.CustomContainer.IoC
{
    [AttributeUsage(AttributeTargets.Method)]
    public class InjectionMethodAttribute : Attribute
    {
        
    }
}
