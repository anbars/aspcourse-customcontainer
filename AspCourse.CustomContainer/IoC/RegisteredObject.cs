﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AspCourse.CustomContainer.IoC
{
    public class RegisteredObject
    {

        public RegisteredObject(Type typeToResolve, Type concrete, LifeCycle lifeCycle)
        {
            TypeToResolve = typeToResolve;
            Concrete = concrete;
            LifeCycle = lifeCycle;

            InjectionMethods = concrete.GetMethods(BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.Instance)
                .Where(m => m.GetCustomAttribute<InjectionMethodAttribute>(true) != null)
                .ToList();
        }

        public object Instance { get; private set; }
        public Type TypeToResolve { get; }
        public Type Concrete { get; }
        public LifeCycle LifeCycle { get; }
        public IEnumerable<MethodInfo> InjectionMethods { get; }

        public void CreateInstance(params object[] args)
        {
            this.Instance = Activator.CreateInstance(this.Concrete, args);
        }
    }
}
