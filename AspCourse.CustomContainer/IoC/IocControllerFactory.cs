﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace AspCourse.CustomContainer.IoC
{
    public class IocControllerFactory : DefaultControllerFactory
    {
        public IocControllerFactory(IContainer container)
        {
            Container = container;
        }

        public IContainer Container { get; }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return Container.Resolve(controllerType) as IController;
        }
    }
}
