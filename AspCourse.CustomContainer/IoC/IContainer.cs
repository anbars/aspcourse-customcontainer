﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspCourse.CustomContainer.IoC
{
    public interface IContainer
    {
        void Register<TTypeToresolve, TConcrete>();
        void Register<TTypeToresolve, TConcrete>(LifeCycle lifeCycle);
        TTypeToresolve Resolve<TTypeToresolve>();
        object Resolve(Type typeToResolve);
    }
}
