﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspCourse.CustomContainer.IoC
{
    public enum LifeCycle
    {
        Singleton,
        Transient
    }
}