﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AspCourse.CustomContainer.IoC
{
    public class IoCDependencyResolver : IDependencyResolver
    {
        public IoCDependencyResolver(IContainer container)
        {
            Container = container;
        }

        public IContainer Container { get; }

        public object GetService(Type serviceType)
        {
            return Container.Resolve(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return new[] { GetService(serviceType) };
        }
    }
}
