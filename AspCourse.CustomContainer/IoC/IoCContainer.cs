﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspCourse.CustomContainer.IoC
{
    public class IoCContainer : IContainer
    {
        private readonly Dictionary<Type, RegisteredObject> _registeredObjects = new Dictionary<Type, RegisteredObject>();
        public void Register<TTypeToresolve, TConcrete>()
        {
            Register<TTypeToresolve, TConcrete>(LifeCycle.Singleton);
        }

        public void Register<TTypeToresolve, TConcrete>(LifeCycle lifeCycle)
        {
            _registeredObjects.Add(typeof(TTypeToresolve), new RegisteredObject(typeof(TTypeToresolve), typeof(TConcrete), lifeCycle));
        }

        public TTypeToresolve Resolve<TTypeToresolve>()
        {
            return (TTypeToresolve)Resolve(typeof(TTypeToresolve));
        }

        public object Resolve(Type typeToResolve)
        {
            var registeredObject = _registeredObjects[typeToResolve];
            if(registeredObject.Instance == null || registeredObject.LifeCycle == LifeCycle.Transient)
            {
                return CreateInstance(registeredObject);
            }
            return registeredObject.Instance;
        }

        private object CreateInstance(RegisteredObject registeredObject)
        {
            var ctor = registeredObject.Concrete.GetConstructors()
                .Select(c => new { Ctor = c, Parameters = c.GetParameters().ToList() })
                .OrderByDescending(c => c.Parameters.Count)
                .FirstOrDefault(c => c.Parameters.All(p => _registeredObjects.ContainsKey(p.ParameterType)));

            if (ctor == null) throw new Exception();

            var parameters = ctor.Parameters.Select(p => Resolve(p.ParameterType));

            registeredObject.CreateInstance(parameters.ToArray());
            var instance = registeredObject.Instance;

            foreach (var method in registeredObject.InjectionMethods)
            {
                var injectionParams = method.GetParameters().Select(p => Resolve(p.ParameterType));
                method.Invoke(instance, injectionParams.ToArray());
            }

            return instance;
        }
    }
}
