﻿using AspCourse.CustomContainer.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspCourse.CustomContainer.Controllers
{
    public class HomeController : Controller
    {
        private IDoNothingService _doNothingService;

        public HomeController()
        {
        }

        [InjectionMethod]
        public void SetService(IDoNothingService doNothingService)
        {
            this._doNothingService = doNothingService;
        }

        public ActionResult Index()
        {
            _doNothingService.DoNotihng();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}